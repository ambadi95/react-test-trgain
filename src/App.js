import React from "react";
import { Route, Switch } from "react-router-dom";

import "./App.css";
import Box from "@mui/material/Box";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import welcomeIcon from "../src/assets/background/bg.png";
import Login from "./components/login";
import Dashboard from "./screens/dashboard";

function App() {
  const history = useHistory();
  const { token, loading } = useSelector((state) => state.auth);
  return (
    <Box>
      <Switch>
        <div className="mainDiv">
          <Route path="/" exact>
            <img
              src={welcomeIcon}
              alt="new"
              style={{ width: "60%", height: "109.4%" }}
            />
            <Box>
              <Login />
            </Box>
          </Route>
          <Route path="/dashboard" render={() => <Dashboard auth={token} />} />
          {token && history.push(`/dashboard/dashb`)}
        </div>
      </Switch>
    </Box>
  );
}

export default App;
