import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { adminLoginRequest, graphRequest } from "../services/services";

const initialState = {
  token: null,
  loading: false,
  graphData: {},
  table: [],
  status: "",
  error: "",
};

export const signinUser = createAsyncThunk(
  "authentication/signinUser",
  async (obj) => {
    const result = await adminLoginRequest("auth/jwt/create/", obj);
    return result;
  }
);

export const getGraphData = createAsyncThunk(
  "authentication/graphData",
  async (token) => {
    const result = await graphRequest("api/dashboard/pie-chart/", token);
    return result;
  }
);

export const getTableData = createAsyncThunk(
  "authentication/TabelData",
  async (token) => {
    const result = await graphRequest("api/contact/", token);
    return result;
  }
);

export const authSlice = createSlice({
  name: "authentication",
  initialState,
  reducers: {
    logout: (state, action) => {
      state.token = null;
      localStorage.clear();
      console.log("logged out :" + state.token);
    },
    addToken: (state, action) => {
      state.token = localStorage.getItem("token");
    },
    clearState: (state, action) => {
      state.error = "";
    },
  },
  extraReducers: {
    [signinUser.pending]: (state, action) => {
      state.loading = true;
    },
    [signinUser.fulfilled]: (state, action) => {
      if (action.payload.status === "false") {
        state.error = action.payload.status;
        state.loading = false;
        alert(action.payload.message);
      } else {
        state.error = null;
        state.token = action.payload.data.access;
        console.log(action.payload);
        localStorage.setItem("token", action.payload.data.access);
        state.loading = false;
      }
    },
    [signinUser.rejected]: (state, action) => {
      console.log("rejected");
      alert("Something went wrong ");
      state.loading = false;
    },

    [getGraphData.pending]: (state, action) => {
      state.loading = true;
    },
    [getGraphData.fulfilled]: (state, action) => {
      if (action.payload.status === "false") {
        state.error = action.payload.status;
        state.loading = false;
        alert(action.payload.status);
      } else {
        state.error = null;
        state.graphData = action.payload.data;
        console.log(action.payload);
        state.loading = false;
      }
    },
    [getGraphData.rejected]: (state, action) => {
      console.log("rejected");
      alert("Something went wrong ");
      state.loading = false;
    },
    [getTableData.pending]: (state, action) => {
      state.loading = true;
    },
    [getTableData.fulfilled]: (state, action) => {
      if (action.payload.status === "false") {
        state.error = action.payload.status;
        state.loading = false;
        alert(action.payload.status);
      } else {
        state.error = null;
        state.table = action.payload.data.results;
        console.log(action.payload);
        state.loading = false;
      }
    },
    [getTableData.rejected]: (state, action) => {
      console.log("rejected");
      alert("Something went wrong ");
      state.loading = false;
    },
  },
});

export default authSlice.reducer;

export const { logout, clearState } = authSlice.actions;
