import Box from "@mui/material/Box";
import { useState } from "react";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { signinUser } from "../app/auth";

function Login() {
  const [username, setUsername] = useState();
  const [password, setpassword] = useState();
  const dispatch = useDispatch();
  const history = useHistory();

  const signin = () => {
    dispatch(
      signinUser({
        user: username,
        pass: password,
      })
    );
  };

  return (
    <Box display="flex" minHeight="100vh" flexDirection="column">
      <Typography
        variant="h5"
        color="#109CF1"
        sx={{
          marginBottom: 2,
          marginLeft: 20,
          marginTop: 30,
          fontWeight: 600,
          fontSize: 44,
        }}
      >
        SaaS Kit
      </Typography>
      <Typography
        variant="h10"
        sx={{
          marginLeft: 20,
          marginTop: 1,
          fontWeight: 700,
          fontSize: 26,
          fontFamily: "Poppins",
        }}
      >
        Hello!
      </Typography>
      <Typography
        variant="h10"
        sx={{
          marginLeft: 20,
          marginTop: 1,
          fontFamily: "Poppins",
          fontSize: 18,
          marginBottom: 5,
        }}
      >
        Signup to get started
      </Typography>

      <TextField
        id="outlined-name"
        label="Email Address"
        sx={{
          marginLeft: 20,
          marginTop: 1,
        }}
        //value={name}
        onChange={(value) => {
          setUsername(value.target.value);
        }}
      />
      <TextField
        id="outlined-name"
        label="Password"
        sx={{
          marginLeft: 20,
          marginTop: 1,
          height: 60,
          width: 307,
        }}
        //value={name}
        onChange={(value) => {
          setpassword(value.target.value);
        }}
      />
      <Button
        sx={{
          marginLeft: 20,
          marginTop: 1,
          height: 57,
        }}
        variant="contained"
        onClick={() => signin()}
      >
        Login
      </Button>
    </Box>
  );
}

export default Login;
