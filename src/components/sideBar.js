import React from "react";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";

import Typography from "@mui/material/Typography";

import { NavLink } from "react-router-dom";

import "../css/Dashboard.css";

export default function DraweItems() {
  const navBarList = [
    { label: "Dashboard", link: "/dashboard/dashb" },
    { label: "Task", link: "/dashboard/projects" },
    { label: "Email", link: "/dashboard/projects" },
    { label: "Contacts", link: "/dashboard/contacts" },
    { label: "Chat", link: "/dashboard/projects" },
    { label: "Deals", link: "/dashboard/projects" },
  ];

  return (
    <div className="drawerItems">
      <Box role="presentation" style={{ height: "100%" }}>
        <Box>
          <Typography
            variant="h5"
            color="#109CF1"
            sx={{
              marginBottom: 2,
              marginLeft: 2,
              marginTop: 2,
              fontWeight: 600,
              fontSize: 18,
            }}
          >
            SaaS Kit
          </Typography>
          <Divider />
        </Box>
        <Box>
          <Typography
            variant="h5"
            color="black"
            sx={{
              marginLeft: 2,
              marginTop: 2,
              fontWeight: "bold",
              fontSize: 14,
              fontFamily: "Poppins",
            }}
          >
            Admin
          </Typography>
          <Typography
            variant="h5"
            color="grey"
            sx={{
              marginBottom: 2,
              marginLeft: 2,
              fontWeight: 500,
              fontSize: 11,
              fontFamily: "Poppins",
            }}
          >
            admin@admin.com
          </Typography>
        </Box>
        <List>
          {navBarList.map((item, index) => (
            <ListItem button component={NavLink} key={index} to={item.link}>
              <Typography
                variant="h5"
                color="#109CF1"
                sx={{
                  fontWeight: 500,
                  fontSize: 13,
                  fontFamily: "Poppins",
                }}
              >
                {item.label}
              </Typography>
            </ListItem>
          ))}
        </List>
        <Divider />
        <Typography
          variant="h5"
          color="#109CF1"
          sx={{
            marginBottom: 2,
            marginLeft: 2,
            marginTop: 2,
            fontWeight: 500,
            fontSize: 13,
            fontFamily: "Poppins",
          }}
        >
          Settings
        </Typography>
      </Box>
    </div>
  );
}
