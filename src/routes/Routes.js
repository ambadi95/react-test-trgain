import { Box } from "@mui/system";
import { useSelector } from "react-redux";

import { Route, Switch } from "react-router-dom";
import Contacts from "../screens/contacts";
import Dash from "../screens/dash";

function Routes() {
  const { token } = useSelector((state) => state.auth);

  return (
    <div>
      <Box>
        <Switch>
          <Route path="/dashboard/dashb" render={() => <Dash />} />
          <Route path="/dashboard/contacts" component={() => <Contacts />} />
        </Switch>
      </Box>
    </div>
  );
}

export default Routes;
