import { DataGrid } from "@mui/x-data-grid";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getTableData } from "../app/auth";

const columns = [
  { field: "id", headerName: "ID", width: 50 },
  { field: "name", headerName: "Name", width: 200 },
  { field: "email", headerName: "Email", width: 200 },
  {
    field: "company_name",
    headerName: "Company Name",
    width: 150,
  },
  {
    field: "role",
    headerName: "Role",
    width: 100,
  },
  {
    field: "forecast",
    headerName: "ForeCast",
    width: 140,
  },
  {
    field: "recent_activity",
    headerName: "Recent Activity",
    width: 160,
  },
];

function Contacts() {
  const { token, table, loading } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTableData(token));
  }, [dispatch]);

  return (
    <div style={{ height: 400, width: "100%" }}>
      {loading ? (
        <p>Loading..</p>
      ) : (
        <DataGrid
          rows={table}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
          checkboxSelection
        />
      )}
    </div>
  );
}

export default Contacts;
