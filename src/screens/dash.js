import { Chart } from "react-google-charts";
import Box from "@mui/material/Box";
import { Switch, Route, withRouter } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getGraphData } from "../app/auth";

function Dash() {
  const { token, graphData } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const apicall = async () => {
    await dispatch(getGraphData(token));
  };

  useEffect(() => {
    apicall();
  }, [dispatch, token]);

  return (
    <Box
      sx={{
        marginBottom: 2,
        marginLeft: 10,
        marginTop: 10,
      }}
    >
      {graphData == null ? (
        <div>Loading Chart</div>
      ) : (
        <Chart
          width={"500px"}
          height={"500px"}
          chartType="PieChart"
          loader={<div>Loading Chart</div>}
          data={[
            ["Task", "Data"],
            ["active", graphData.active],
            ["completed", graphData.completed],
            ["ended", graphData.ended],
          ]}
          options={{
            title: "Task",
            pieHole: 0.9,
          }}
          rootProps={{ "data-testid": "3" }}
        />
      )}
    </Box>
  );
}

export default Dash;
