import DraweItems from "../components/sideBar";
import { Redirect } from "react-router-dom";

import "../css/Dashboard.css";
import Routes from "../routes/Routes";
import DashboardHeader from "../components/DashboardHeader";

function Dashboard(auth) {
  // if (auth === null) {
  //   return <Redirect to="/" />;
  // }

  return (
    <div>
      <div className="mainDiv">
        <div className="child_div">
          <DraweItems />
        </div>
        <div className="div2">
          <DashboardHeader />
          <Routes />
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
