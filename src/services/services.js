const url = "http://saaskit.tegain.com/";

export const adminLoginRequest = async (api, obj) => {
  const res = await fetch(url + api, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: obj.user,
      password: obj.pass,
    }),
  });

  return await res.json();
};

export const graphRequest = async (api, token) => {
  const res = await fetch(url + api, {
    method: "GET",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: "Bearer " + token,
    },
  });
  const data = await res.json();
  if (data.code === 401) {
    return data;
  } else {
    return data;
  }
};
